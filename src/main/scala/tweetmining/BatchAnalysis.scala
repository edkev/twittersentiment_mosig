package tweetmining

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object BatchAnalysis {
  val conf = new SparkConf().setMaster("local[6]").setAppName("Spark batch processing")
  val sc = new SparkContext(conf)
  sc.setLogLevel("WARN")
  def main(args: Array[String]): Unit = {
    println("Twitter data batch processing")
    val tweets: RDD[String] = sc.textFile("1Mtweets_en.txt")
    tweets.cache()
    //*****1ère question*****
    println("count: " + tweets.filter(x=> x.contains("Donald Trump")).count())
    //*****2è question*****
    println("(tweet, sentiment)")
    val pairTweetSent = tweets.map(x=>(x, TweetUtilities.getSentiment(x)))
    pairTweetSent.cache()
    pairTweetSent.take(5).foreach(println)
    //*****3è question*****
    val pairMentionOrHashSent =  pairTweetSent.flatMap(
      x=> (TweetUtilities.getHashTags(x._1)++TweetUtilities.getMentions(x._1))
        .map(y=>(y,x._2)))
    pairMentionOrHashSent.take(5).foreach(println)
    //*****4è question*****
    val order = pairMentionOrHashSent.map(_.swap)
    println("top 5 most positives")
    order.top(5).foreach(println)
    println("top 5 most negatives")
    order.takeOrdered(5).foreach(println)



  }
}