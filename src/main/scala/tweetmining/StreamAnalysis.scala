package tweetmining

import java.util.Collections

import org.apache.spark.rdd.RDD
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.dstream.DStream
import org.apache.kafka.clients.consumer.{ConsumerRecord, KafkaConsumer}
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.dstream.DStream


object StreamAnalysis {
  val conf = new SparkConf().setMaster("local[6]").setAppName("Spark batch processing")
  val sc = new SparkContext(conf)
  val streamingContext = new StreamingContext(sc, Seconds(10))
  sc.setLogLevel("WARN")
  def main(args: Array[String]): Unit = {
    println("Twitter data stream processing")
    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "localhost:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "twitter-consumer",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean))

    val topics = Array("tweets")
    val tweetStream = KafkaUtils.createDirectStream[String, String](
      streamingContext,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams))

    //add your code here
    var containsDonaldTrump: Long = 0
    var pairTweetSent = sc.emptyRDD[(String, Double)]

    tweetStream.foreachRDD(rdd => {

      val tweets = rdd.flatMap(record => record.value().split("\\n"))

      tweets.cache()
      //*****1ère question*****
      containsDonaldTrump = containsDonaldTrump + tweets.filter(x=> x.contains("Donald Trump")).count
      println(s"$containsDonaldTrump tweets contiennent Donald Trump")

      //*****2è question*****
      pairTweetSent = pairTweetSent.union(tweets.map(x=>(x, TweetUtilities.getSentiment(x))))

      pairTweetSent.take(5).foreach(println)

      //*****3è question*****

      val pairMentionOrHashSent =  pairTweetSent.flatMap(
        x=> (TweetUtilities.getHashTags(x._1)++TweetUtilities.getMentions(x._1))
          .map(y=>(y,x._2)))

      //*****4è question*****
      val order = pairMentionOrHashSent.map(_.swap)
      println("top 5 most positives")
      order.top(5).foreach(println)
      println("top 5 most negatives")
      order.takeOrdered(5).foreach(println)

    })


    streamingContext.start()
    streamingContext.awaitTermination()
  }
}